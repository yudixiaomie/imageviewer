package cn.yudi.imageviewer.widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by yudi on 2017/3/2.
 * Prevent gutter drag
 */

public class YuViewPager extends ViewPager implements YuImageView.DraggingListener{
    int mGutterSize;

    public YuViewPager(Context context) {
        this(context, null);
    }

    public YuViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mGutterSize = 44;
    }

    private float mLastMotionX;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                mLastMotionX = ev.getX();
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                final float x = ev.getX();
                final float dx = x - mLastMotionX;
                if (isGutterDrag(mLastMotionX, dx)) {
                    Log.e("intercept", "action: " + "gutter drag");
                    return false;
                }
                break;
            }
        }
        return super.onInterceptTouchEvent(ev);
    }

    private boolean isGutterDrag(float x, float dx) {
        return (x < mGutterSize && dx > 0) || (x > getWidth() - mGutterSize && dx < 0);
    }

    int alpha = 255;

    @Override
    public void onAlphaChanged(float factor) {
        // from 0 ~ 1
        Log.e("factor", "factor:  " + factor);
        alpha = 255 - (int) (50 * factor);
        setBackgroundColor(Color.argb(alpha, 0, 0, 0));
    }

    @Override
    public void onResetAlpha() {
        alpha = 255;
        setBackgroundColor(Color.argb(alpha, 0, 0, 0));
    }

    @Override
    public boolean needTransition() {
        return (255 - alpha) / (0.F + 50) > 0.35;
    }

    @Override
    public boolean startTransition() {
        if (listener != null) {
            listener.onBackPress();
            return true;
        }
        return false;
    }

    OnTransitionListener listener;

    public void setTransitionListener(OnTransitionListener listener) {
        this.listener = listener;
    }

    public interface OnTransitionListener {
        void onBackPress();
    }
}
