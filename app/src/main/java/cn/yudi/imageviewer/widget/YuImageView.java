package cn.yudi.imageviewer.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.OverScroller;

/**
 * Created by yudi on 2017/2/14.
 * <p>
 * Features
 * <p>
 * 1. Flinging, Pinch-Zoom, Double-Click-to-Zoom, Dragging, Auto-position
 * <p>
 * 2. Partially-loaded images(save memory)
 * <p>
 * 3. Can load the very long image
 * <p>
 * macbook上快捷键的学习
 * 如何补全代码 shift+control+space长按
 * 如何调出设置command+，
 * 如何重命名名字 shift+fn+F6
 * 文件中查找关键词用command+F
 * 文件中替代关键词用command+R
 * 取消边栏，用shift+esc
 * 查看方法的Hierarchy，用control+H
 * 查看方法的文档，用control+J
 * 局部变量变成全局变量alt+command+F
 * <p>
 * 思路
 * 以前的做法是在别人setImageBitmap()或者setImageDrawable()或者setImageResource()的时候，得到图片的原始高度
 * 获得图片的原始高度可以得到最开始缩放的比例oriScale
 * 然后初始化矩阵
 * 这个矩阵是用于当scaleType为matrix的时候可以通过matrix来改变图片的位于控件中的位置和大小
 * 先不管大图片一次性载入会占用很多内存的问题---待定！！！！！！！！！
 * <p>
 * bitmap的问题
 * 图片放在不同的drawable目录里面(如hdpi或者xhdpi)，decode出来的bitmap的宽高是不同，是因为放在不同的drawable目录里面，图片会根据dp和px对应的关系相应地放大和缩小
 * <p>
 * 不用Animator来实现渐变的效果，用View自带的computeScroll来实现，是为了减少其他类的引入
 * 引入的问题，如何计算因为放大而要重新计算的位移
 * 可以通过用matrix变换（matrix.postScale(sx,sy,px,py)，先看看最终的trans是多少，然后再从现在的trans变换过去
 * <p>
 * 放大bug---解决
 * 突然出现的bug
 * 图片放大后虽然图片总体的大小大于控件的大小，但是还是出现了白边
 * <p>
 * 漂移bug---解决
 * 出现了一个和touch事件有关的bug
 * 多个手指接触屏幕后，松开任意一个手指都会出现漂移现象
 * 主要原因是firstPointer的坐标没有跟着变化
 * <p>
 * 新bug---解决
 * 原因是activePointerId不是0，导致第二个手指放下去的时候，originalDistance为0
 * 出现的原因是primary pointer抬起来后，再放下来，就缩放不了了
 * <p>
 * 缩放bug---解决
 * 原因是开始缩放前的originalScale是originalValues[Matrix.SCALEX]，而不是m[Matrix.SCALEX],以originalValues[Matrix.SCALEX]作为
 * 基础会造成先缩小，后放大的样子
 * 当图片的大小小于宽高的时候，双击缩小回去，出现了先放大后缩小的情况
 * <p>
 * 新增功能---解决
 * 新的问题，因为双手缩放会造成有白边，要用动画过度回来
 * <p>
 * 问题---解决
 * 双指缩放的时候，假如有白边，松开一个手指的时候会出现图片瞬间移动到控件的边界
 * 原因是松开手指的一瞬间会触发dragging()的方法，因为dragging()里面的方法判断不是很严谨，造成deltaX或者deltaY=空白的大小，直接移动过去了
 * <p>
 * 新增功能---解决
 * 如何增加多指缩放的效果，，微信都没有做出来，qq有做出来
 * 主要是思路
 * 算出中心点（解析几何，坐标相加后除以总手指数量）
 * 算出中心点到每个手指的距离，然后全部加起来，作为最开始的originalDistance
 * 然后每次调用zooming(scale)方法的时候，都要重新计算每一个手指到中心点的距离，然后加起来除以originalDistance得出相比于最开始的originalScale,现在的倍数是多少
 * 引入的新问题，如果有一个手指抬起来，就会造成originalDistance不准，下一次调用zooming(scale)方法的时候就会计算出一个错误的倍数
 * 解决方法是
 * 在ACTION_POINTER_UP的时候更新originalDistance(不是更新originalDistance为少一个手指时候的总的distance)
 * 而是保持zooming(scale)中的参数scale不变的前提下，把分母的originalDistance改变，这样就可以过渡很自然了
 * <p>
 * 引入的bug---解决
 * 当所有的手指都在中心点的同一侧的时候，容易出现先缩小后放大的问题，如何解决
 * 每次缩放完成后，需要更新midPoint达到不会有手指都在同一边的情况
 * <p>
 * 因为上面的问题引入的bug---解决
 * 会导致松开某个手指的时候，出现图片shaking
 * 原因是松开手指后midpoint的位置会改变，因为midpoint计算的距离要重新算
 * 解决办法是在action—pointer-up的时候，去除要抬起的手指，重新算出新的midpoint即可
 * <p>
 * pointerIndex和pointerId的区别---不是很清楚
 * pointerIndex在event中的位置不变，但是pointerId是经常变的，因为有些事件不是所有的pointer都有比如pointer_up等
 * <p>
 * 步骤1： 先把图片居中在控件，计算出这时候图片的缩放比列和位移
 * 步骤2： 解决双击放大缩小的功能,问题关键所在是matrix对应的不是单个图片对于控件的matrix，而是整个控件对应一个matrix，包括空白区域
 * 步骤3： 解决拖拽的功能
 * 步骤4： 解决双指缩放问题
 * 步骤5： 解决自动translate问题
 * 步骤6： 解决多指缩放问题
 * 步骤7： 解决fling的问题
 * <p>
 * 拓展1： 可以增加拖拽的回弹
 * 拓展2： 增加fling的overScroll，解决了边界fling和拖拽回弹的冲突
 * 拓展3： 增加缩放回弹
 */


public class YuImageView extends android.support.v7.widget.AppCompatImageView {
    String TAG = "YUImageView";
    static final int RANGE = 100;
    static final int MAX_SCALE = 2;
    static final int MIN_SCALE = 1;
    static final float SUPER_MIN_SCALE = 0.7F;
    static final int SUPER_MAX_SCALE = 3;
    static final int STATE_IDLE = 0;
    static final int STATE_ZOOM_OUT = 1;
    static final int STATE_ZOOM_IN = 2;
    static final int STATE_TRANSLATE = 3;
    static final int STATE_FLING = 4;
    static final int STATE_TRANSLATE_AND_SCALE_BIG = 5;
    static final int STATE_TRANSLATE_AND_SCALE_SMALL = 6;
    int state = STATE_IDLE;
    PointF firstPoint = new PointF();
    PointF midPoint = new PointF();
    PointF startAlphaPoint = new PointF();
    private float originalDistance;
    private float originalScale;
    private int overScrollX;
    private int overScrollY;
    private boolean canNestedScroll = true;
    private boolean needBackTransition = false;
    private boolean startDraggingAndScale = false;
    private int alphaTotalDistance;

    public YuImageView(Context context) {
        this(context, null);
    }

    public YuImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public YuImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setScaleType(ScaleType.MATRIX);
        gestureDetector = new GestureDetector(context, new GestureListener());
        scroller = new OverScroller(context, new LinearOutSlowInInterpolator());
        overScrollY = overScrollX = context.getResources().getDisplayMetrics().widthPixels / 6;
        alphaTotalDistance = context.getResources().getDisplayMetrics().heightPixels / 2;
        Log.e(TAG, "overScroll: " + overScrollY);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // 判断是要放大还是缩小
            if (m[Matrix.MSCALE_X] - originalValues[Matrix.MSCALE_X] < 0.0001) {
                zoomOut(e);
            } else {
                zoomIn();
            }
            originalScale = m[Matrix.MSCALE_X];
            originalX = m[Matrix.MTRANS_X];
            originalY = m[Matrix.MTRANS_Y];
            postInvalidate();
            return super.onDoubleTap(e);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (draggingListener != null)
                draggingListener.startTransition();
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            float leftBorder = viewX - m[Matrix.MSCALE_X] * pixelX;
            float topBorder = viewY - m[Matrix.MSCALE_Y] * pixelY;
            boolean needAdjust;
            // Why we use the overScrollX / 2 instead of 0
            // 因为我想实现在加入overDragging之后，边界的fling不受之前程序判断的影响
            // 如果是之前的0，则这个fling动作就不行执行，那么边界的滑动就会看上去很卡顿
            // 换成overScroll/2之后，有一段的距离给检测时候是边界滑动，因为加入overScroll之后
            // Fling会把overDragging的地方自动还原回去，所以这样做是可以的
            boolean leftNeedUpdate = (leftBorder <= 0 && m[Matrix.MTRANS_X] > overScrollX / 2 || leftBorder >= 0 && Math.abs(leftBorder / 2 - m[Matrix.MTRANS_X]) > 2);
            boolean rightNeedUpdate = (leftBorder <= 0 && m[Matrix.MTRANS_X] < leftBorder - overScrollX / 2 || leftBorder >= 0 && Math.abs(leftBorder / 2 - m[Matrix.MTRANS_X]) > 2);
            boolean topNeedUpdate = (topBorder <= 0 && m[Matrix.MTRANS_Y] > overScrollY / 2 || topBorder >= 0 && Math.abs(topBorder / 2 - m[Matrix.MTRANS_Y]) > 2);
            boolean bottomNeedUpdate = (topBorder <= 0 && m[Matrix.MTRANS_Y] < topBorder - overScrollY / 2 || topBorder >= 0 && Math.abs(topBorder / 2 - m[Matrix.MTRANS_Y]) > 2);
            needAdjust = (leftNeedUpdate || rightNeedUpdate || topNeedUpdate || bottomNeedUpdate);
            Log.e(TAG, "needAdjust: " + needAdjust);
            if (e2.getPointerCount() == 1 && !needAdjust)
                fling(velocityX, velocityY);
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    float finalX = 0;
    float finalY = 0;
    float originalX = 0;
    float originalY = 0;

    /**
     * 四个动画都要用到这个关键的方法
     * 第一个： 双击放大，需要的参数有x坐标和y坐标和放大的倍数
     * 第二个： 双击缩小，需要的参数有x坐标和y坐标和缩小的倍数
     * 第三个： 平移图片，需要的参数有x坐标和y坐标和不变的倍数
     * 第四个： Fling滑动
     */
    @Override
    public void computeScroll() {
        if (!scroller.isFinished() && scroller.computeScrollOffset()) {
            Matrix imageMatrix = getImageMatrix();
            int startX = scroller.getStartX();
            int currX = scroller.getCurrX();
            float factor = (currX - startX + 0.f) / RANGE;// From 0 ~ 1
            float scale = m[Matrix.MSCALE_X];// 需要放大的倍数
            float transX = originalX + factor * (finalX - originalX);
            float transY = originalY + factor * (finalY - originalY);
            Log.e(TAG, "factor: " + factor);
            switch (state) {
                case STATE_ZOOM_OUT:
                    scale = originalScale + (originalValues[Matrix.MSCALE_X] * MAX_SCALE - originalScale) * factor;
                    break;
                case STATE_ZOOM_IN:
                    scale = originalScale + (originalValues[Matrix.MSCALE_X] * MIN_SCALE - originalScale) * factor;
                    break;
                case STATE_TRANSLATE:
                    break;
                case STATE_TRANSLATE_AND_SCALE_BIG:
                    scale = originalScale + (originalValues[Matrix.MSCALE_X] * MIN_SCALE - originalScale) * factor;
                    break;
                case STATE_TRANSLATE_AND_SCALE_SMALL:
                    scale = originalScale - (originalScale - originalValues[Matrix.MSCALE_X] * MAX_SCALE) * factor;
                    break;
                case STATE_FLING:
                    transX = scroller.getCurrX();
                    transY = scroller.getCurrY();
                    Log.e(TAG, "startX: " + startX + ", transX: " + transX);
                    break;
            }
            imageMatrix.setScale(scale, scale, 0, 0);
            imageMatrix.postTranslate(transX, transY);
            setImageMatrix(imageMatrix);
            if (!scroller.isFinished()) {
                postInvalidate();
            }
        }
    }

    OverScroller scroller;
    private GestureDetector gestureDetector;
    private Matrix matrix = new Matrix();
    private float[] originalValues = new float[9];//The original values of the view after the picture has been center
    private float[] m = new float[9];

    @Override
    public void setImageMatrix(Matrix matrix) {
        super.setImageMatrix(matrix);
        updateMatrix(matrix);
        Log.e(TAG, "image_matrix: " + (matrix == getImageMatrix()));
        printMatrixValues("setImageMatrix", m);
    }

    @Override
    public Matrix getImageMatrix() {
        return super.getImageMatrix();
    }

    void updateMatrix(Matrix matrix) {
        this.matrix.set(matrix);// Update the matrix when
        this.matrix.getValues(m);// Update the matrix values
    }

    public void printMatrixValues(String tag, float[] m) {
        Log.e(TAG, tag + "Matrix.MTRANS_X : " + m[Matrix.MTRANS_X] + ", Matrix.MTRANS_Y : " + m[Matrix.MTRANS_Y]
                + ", Matrix.MSCALE_X : " + m[Matrix.MSCALE_X] + ", Matrix.MSCALE_Y : " + m[Matrix.MSCALE_Y] + ", pixelX: " + pixelX + ", pixelY: " + pixelY);
    }

    private int pixelX = -1;// The width of the picture
    private int pixelY = -1;// The height of the picture
    private int viewX = -1;// The width of the view
    private int viewY = -1;// The height of the view

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        pixelX = drawable.getIntrinsicWidth();
        pixelY = drawable.getIntrinsicHeight();
        centerPicture();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        pixelX = bm.getWidth();
        pixelY = bm.getHeight();
        centerPicture();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        Bitmap bm = BitmapFactory.decodeResource(getContext().getResources(), resId);
        pixelX = bm.getWidth();
        pixelY = bm.getHeight();
        bm.recycle();
        centerPicture();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int sizeX = MeasureSpec.getSize(widthMeasureSpec);
        int sizeY = MeasureSpec.getSize(heightMeasureSpec);
        // TODO: 2017/2/28 use this method for not shading in th transition, find a good method to avoid flickering
        if (sizeX > viewX || sizeY > viewY) {
            viewX = viewX > sizeX ? viewX : sizeX;
            viewY = viewY > sizeY ? viewY : sizeY;
            Log.e(TAG, "viewX: " + viewX + ", viewY: " + viewY);
            centerPicture();
        }
    }

    void centerPicture() {
        if (pixelY == -1 || pixelX == -1 || viewX == -1 || viewY == -1) return;
        float scaleX = viewX / (0.f + pixelX);
        float scaleY = viewY / (0.f + pixelY);
        float scale = Math.min(scaleX, scaleY);
        Matrix imageMatrix = getImageMatrix();
        imageMatrix.setScale(scale, scale, 0, 0);//Center x
        imageMatrix.postTranslate((viewX - scale * pixelX) / 2, (viewY - scale * pixelY) / 2);//Center y
        setImageMatrix(imageMatrix);
        imageMatrix.getValues(m);
        imageMatrix.getValues(originalValues);//Save the original values
        printMatrixValues("centerPicture", m);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        int pointerCount = event.getPointerCount();
        int pointers = event.getPointerCount();
        Log.e(TAG, "action: " + event.getActionMasked());
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (state == STATE_FLING && !scroller.isFinished()) {
                    scroller.abortAnimation();
                }
                if (m[Matrix.MSCALE_X] <= (originalValues[Matrix.MSCALE_X] + 0.001)) {
                    startAlphaPoint.set(event.getX(), event.getY());
                }
                needBackTransition = false;
                canNestedScroll = true;// Prevent some wired situation
                startDraggingAndScale = false;//
                firstPoint.set(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                // Only care the secondary pointer, don't care the others
                // And the center point
                Log.e(TAG, "pointers: " + event.getPointerCount());
                midPoint.set(getNewMidPoint(event));
                originalDistance = getNewTotalDistance(event);
                originalScale = m[Matrix.MSCALE_X];
                break;
            case MotionEvent.ACTION_MOVE:
                if (pointers == 1) {
                    float dx = (event.getX() - firstPoint.x);
                    float dy = (event.getY() - firstPoint.y);
                    if (m[Matrix.MSCALE_X] <= (originalValues[Matrix.MSCALE_X] + 0.001)) {
                        if (canNestedScroll || Math.abs(dy) > Math.abs(dx) || startDraggingAndScale) {
                            if (Math.abs(dy) > Math.abs(dx))
                                startDraggingAndScale = true;
                            draggingAndScaling(dx, dy);
                        }
                    } else {
                        if (canNestedScroll)
                            dragging(dx, dy);
                    }
                    firstPoint.set(event.getX(), event.getY());
                } else if (pointers >= 2) {
                    midPoint.set(getNewMidPoint(event));// Update the mid point every time when the pinch zoom is over
//                    float dx = midPoint.x - oldMidPointer.x;
//                    float dy = midPoint.y - oldMidPointer.y;
                    zooming(getNewTotalDistance(event) / originalDistance);
                }
                break;
            case MotionEvent.ACTION_POINTER_UP:
                midPoint.set(getNewMidPoint(event));
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                // update the originalDistance for not shaking
                float upPointerDistance = (float) Math.sqrt((event.getX(pointerIndex) - midPoint.x) * (event.getX(pointerIndex) - midPoint.x)
                        + (event.getY(pointerIndex) - midPoint.y) * (event.getY(pointerIndex) - midPoint.y));
                originalDistance = getNewTotalDistance(event) - upPointerDistance;
                originalScale = m[Matrix.MSCALE_X];
                // firstPointer的坐标应该更改为没有抬起来的手指的坐标
                // 否则会对dragging的时候造成漂移的影响
                if (pointerCount == 2) {
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    firstPoint.set(MotionEventCompat.getAxisValue(event, MotionEventCompat.AXIS_X, newPointerIndex),
                            MotionEventCompat.getAxisValue(event, MotionEventCompat.AXIS_Y, newPointerIndex));
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e(TAG, "cancel");
            case MotionEvent.ACTION_UP:
                if (draggingListener != null && needBackTransition) {
                    draggingListener.startTransition();
                } else {
                    translateAndScale();
                    if (draggingListener != null)
                        draggingListener.onResetAlpha();
                }
                break;
        }
        return true;//super.onTouchEvent(event);
    }

    PointF getNewMidPoint(MotionEvent event) {
        boolean changing = event.getActionMasked() == MotionEvent.ACTION_POINTER_UP && event.getPointerCount() > 2;
        final int pointerIndex = MotionEventCompat.getActionIndex(event);
        int pointerCount = event.getPointerCount();
        float centerX = 0;
        float centerY = 0;
        for (int i = 0; i < pointerCount; i++) {
            if (changing) {
                if (i == pointerIndex)
                    continue;
            }
            centerX += event.getX(i);
            centerY += event.getY(i);
        }
        int count = changing ? (pointerCount - 1) == 0 ? 1 : (pointerCount - 1) : pointerCount;
        return new PointF(centerX / count, centerY / count);
    }

    float getNewTotalDistance(MotionEvent event) {
        float totalDistance = 0;
        int pointerCount = event.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            float disX = event.getX(i) - midPoint.x;
            float disY = event.getY(i) - midPoint.y;
            totalDistance += (float) Math.sqrt(disX * disX + disY * disY);
        }
        return totalDistance;
    }

    void draggingAndScaling(float dx, float dy) {
        Matrix imageMatrix = getImageMatrix();
        imageMatrix.postTranslate(dx, dy);
        float disX = firstPoint.x - startAlphaPoint.x;
        float disY = firstPoint.y - startAlphaPoint.y;
        float factor = (float) (Math.sqrt(disX * disX + disY * disY) / alphaTotalDistance);
        factor = Math.max(0, Math.min(1, factor));
        Log.e(TAG, "alpha factor: " + factor);
        if (draggingListener != null) {
            needBackTransition = draggingListener.needTransition();
            draggingListener.onAlphaChanged(factor);
        }
        setImageMatrix(imageMatrix);
        postInvalidate();
    }

    void fling(float velocityX, float velocityY) {
        int leftBorder = (int) (viewX - m[Matrix.MSCALE_X] * pixelX);
        int topBorder = (int) (viewY - m[Matrix.MSCALE_Y] * pixelY);
        int minX = leftBorder > 0 ? (int) m[Matrix.MTRANS_X] : leftBorder;
        int minY = topBorder > 0 ? (int) m[Matrix.MTRANS_Y] : topBorder;
        int maxX = minX < 0 ? 0 : minX;
        int maxY = minY < 0 ? 0 : minY;
        if (minX != maxX || minY != maxY) {
            float overX = (leftBorder > 0 && Math.abs(m[Matrix.MTRANS_X] - leftBorder / 2) < 2) ? 0 : overScrollX / 2;
            float overY = (topBorder > 0 && Math.abs(m[Matrix.MTRANS_Y] - topBorder / 2) < 2) ? 0 : overScrollY / 2;
            state = STATE_FLING;
            scroller.fling((int) m[Matrix.MTRANS_X], (int) m[Matrix.MTRANS_Y], (int) (velocityX / 1.75F), (int) (velocityY / 1.75F), minX, maxX, minY, maxY, (int) overX, (int) overY);
            postInvalidate();
        }
    }

    void translateAndScale() {
        boolean zoomOutToOriginalSize = m[Matrix.MSCALE_X] < originalValues[Matrix.MSCALE_X];
        boolean zoomInToMaxSize = m[Matrix.MSCALE_X] / originalValues[Matrix.MSCALE_X] > 2;
        float[] tmp = m;
        if (zoomInToMaxSize) {
            // 模拟缩小后的图片，看看有没有超出边界
            Matrix imageMatrix = getImageMatrix();
            float factor = (originalValues[Matrix.MSCALE_X] * MAX_SCALE) / m[Matrix.MSCALE_X];
            imageMatrix.postScale(factor, factor, midPoint.x, midPoint.y);
            tmp = new float[9];
            imageMatrix.getValues(tmp);
        }
        float leftBorder = viewX - tmp[Matrix.MSCALE_X] * pixelX;
        float topBorder = viewY - tmp[Matrix.MSCALE_Y] * pixelY;
        boolean needAdjust;
        // 条件一： 图片的宽小于控件的宽并且不处于中间
        // 条件二： 图片的高小于控件的高并且不处于中间
        // 条件三： 图片大于控件的宽高，但是有左白边，或者有右白边
        // 条件四： 图片大于控件的宽高，但是有上白边，或者有下白边
        boolean xNotCenter = leftBorder >= 0 && (leftBorder / 2 != tmp[Matrix.MTRANS_X]);
        boolean yNotCenter = topBorder >= 0 && (topBorder / 2 != tmp[Matrix.MTRANS_Y]);
        boolean leftNotBorder = leftBorder <= 0 && tmp[Matrix.MTRANS_X] > 0;
        boolean rightNotBorder = leftBorder <= 0 && tmp[Matrix.MTRANS_X] < leftBorder;
        boolean topNotBorder = topBorder <= 0 && tmp[Matrix.MTRANS_Y] > 0;
        boolean bottomNotBorder = topBorder <= 0 && tmp[Matrix.MTRANS_Y] < topBorder;
        needAdjust = (xNotCenter || yNotCenter || leftNotBorder || rightNotBorder || topNotBorder || bottomNotBorder);

        Log.e(TAG, "state: " + state);
        if ((needAdjust && scroller.isFinished()) || zoomInToMaxSize || zoomOutToOriginalSize) {
            originalScale = m[Matrix.MSCALE_X];
            originalX = m[Matrix.MTRANS_X];
            originalY = m[Matrix.MTRANS_Y];
            finalX = zoomOutToOriginalSize ? originalValues[Matrix.MTRANS_X] : // 图片小于最小倍数MIN_SCALE需要先放大
                    xNotCenter ? leftBorder / 2 : leftNotBorder ? 0 : rightNotBorder ? leftBorder : tmp[Matrix.MTRANS_X];
            finalY = zoomOutToOriginalSize ? originalValues[Matrix.MTRANS_Y] : // 图片小于最小倍数MIN_SCALE需要先放大
                    yNotCenter ? topBorder / 2 : topNotBorder ? 0 : bottomNotBorder ? topBorder : tmp[Matrix.MTRANS_Y];
            if (finalX != m[Matrix.MTRANS_X] || finalY != m[Matrix.MTRANS_Y]) {
                Log.e(TAG, "需要更新！");
                state = zoomOutToOriginalSize ? STATE_TRANSLATE_AND_SCALE_BIG :
                        zoomInToMaxSize ? STATE_TRANSLATE_AND_SCALE_SMALL :
                                STATE_TRANSLATE;
                scroller.startScroll(0, 0, RANGE, 0);
                postInvalidate();
            }
        }
    }

    void dragging(float dx, float dy) {
        Log.e(TAG, "canScroll: dx: " + dx);
        Matrix imageMatrix = getImageMatrix();
        // 1. 图片放大或者缩小后的长或宽在控件的范围内，不允许在小于控件范围内的部分滑动
        // 2. 可滑动的部分，不可以超过放大后加上overScroll的边界
        float leftBorder = viewX - m[Matrix.MSCALE_X] * pixelX;
        float deltaX = (m[Matrix.MSCALE_X] * pixelX <= viewX) ? 0 :// 条件一
                // 条件二
                dx > 0 ? (dx + m[Matrix.MTRANS_X]) >= overScrollX ? (overScrollX - m[Matrix.MTRANS_X]) : dx ://从左到右滑动
                        (dx + m[Matrix.MTRANS_X]) <= (leftBorder - overScrollX) ? (leftBorder - overScrollX - m[Matrix.MTRANS_X]) : dx;//从右到左滑动
        float topBorder = (viewY - m[Matrix.MSCALE_Y] * pixelY);
        float deltaY = (m[Matrix.MSCALE_Y] * pixelY <= viewY) ? 0 :// 条件一
                //条件二
                dy > 0 ? (dy + m[Matrix.MTRANS_Y]) >= overScrollY ? (overScrollY - m[Matrix.MTRANS_Y]) : dy ://从上往下滑动
                        ((dy + m[Matrix.MTRANS_Y]) <= (topBorder - overScrollY) ? (topBorder - overScrollY - m[Matrix.MTRANS_Y]) : dy);//从下往上滑动
        Log.e(TAG, "deltaX: " + deltaX + ", deltaY: " + deltaY + ", dx: " + dx + ", dy: " + dy);
        float factorX = 1;
        float factorY = 1;
        if ((m[Matrix.MTRANS_X] > 0 && m[Matrix.MTRANS_X] < overScrollX && deltaX > 0) || // 左边界的拖拽衰减判断
                (m[Matrix.MTRANS_X] < leftBorder && m[Matrix.MTRANS_X] > leftBorder - overScrollX && deltaX < 0))// 右边界的拖拽衰减判断
            factorX = 1 + ((deltaX > 0 ? m[Matrix.MTRANS_X] : (leftBorder - m[Matrix.MTRANS_X])) / overScrollX) * 9;
        if ((m[Matrix.MTRANS_Y] > 0 && m[Matrix.MTRANS_Y] < overScrollY && deltaY > 0) || // 上边界的拖拽衰减判断
                (m[Matrix.MTRANS_Y] < topBorder && m[Matrix.MTRANS_Y] > topBorder - overScrollY && deltaY < 0))// 下边界的拖拽衰减判断
            factorY = 1 + ((deltaY > 0 ? m[Matrix.MTRANS_Y] : (topBorder - m[Matrix.MTRANS_Y])) / overScrollY) * 9;
        imageMatrix.postTranslate(deltaX / factorX, deltaY / factorY);
        setImageMatrix(imageMatrix);
        postInvalidate();
    }

    void zooming(float scale) {
        Log.e(TAG, "scale: " + scale);
        Matrix imageMatrix = getImageMatrix();
        // 这个currentFactor是用于控制scaleX不要有累积作用的
        float currentFactor = originalScale * scale / m[Matrix.MSCALE_X];
        // 条件一 小于最大可缩放倍数SUPER_MAX_SCALE的时候，必须大于最小可缩放倍数SUPER_MIN_SCALE
        // 条件二 必须小于可以放大的最大倍数SUPER_MAX_SCALE
        float factor = originalScale * scale <= originalValues[Matrix.MSCALE_X] * SUPER_MAX_SCALE ?
                originalScale * scale < originalValues[Matrix.MSCALE_X] * SUPER_MIN_SCALE ? originalValues[Matrix.MSCALE_X] * SUPER_MIN_SCALE / m[Matrix.MSCALE_X] : currentFactor :// 条件一
                originalValues[Matrix.MSCALE_X] * SUPER_MAX_SCALE / m[Matrix.MSCALE_X];// 条件二
        imageMatrix.postScale(factor, factor, midPoint.x, midPoint.y);
        setImageMatrix(imageMatrix);
        postInvalidate();
    }

    void zoomOut(MotionEvent e) {
        state = STATE_ZOOM_OUT;
        scroller.startScroll(0, 0, RANGE, 0);
        // 这个方法主要是为了求出双击后放大图片的具体位置
        // 主要是图片左上角的坐标
        Matrix tmpMatrix = new Matrix(getImageMatrix());
        tmpMatrix.postScale(MAX_SCALE * originalValues[Matrix.MSCALE_X] / m[Matrix.MSCALE_X],
                MAX_SCALE * originalValues[Matrix.MSCALE_X] / m[Matrix.MSCALE_X], e.getX(), e.getY());
        float[] mmm = new float[9];
        tmpMatrix.getValues(mmm);
        // 条件一，如果放大后的图片小于控件的宽高，居中
        // 条件二，放大后的图片大于控件的宽高，则要不留白边
        float leftBorder = viewX - mmm[Matrix.MSCALE_X] * pixelX;
        finalX = mmm[Matrix.MSCALE_X] * pixelX <= viewX ?
                leftBorder / 2 : mmm[Matrix.MTRANS_X] > 0 ? 0 : mmm[Matrix.MTRANS_X] < leftBorder ? leftBorder : mmm[Matrix.MTRANS_X];
        float topBorder = viewY - mmm[Matrix.MSCALE_Y] * pixelY;
        finalY = mmm[Matrix.MSCALE_Y] * pixelY <= viewY ?
                topBorder / 2 : mmm[Matrix.MTRANS_Y] > 0 ? 0 : mmm[Matrix.MTRANS_Y] < topBorder ? topBorder : mmm[Matrix.MTRANS_Y];
    }

    void zoomIn() {
        state = STATE_ZOOM_IN;
        scroller.startScroll(0, 0, RANGE, 0);
        finalX = originalValues[Matrix.MTRANS_X];
        finalY = originalValues[Matrix.MTRANS_Y];
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        // direction < 0 from left to right
        // direction >0 from right to left
        canNestedScroll = true;
        float leftBorder = viewX - m[Matrix.MSCALE_X] * pixelX;
        if (leftBorder >= 0) {
            // 居中后就不能滑动了
            canNestedScroll = Math.abs(leftBorder / 2 - m[Matrix.MTRANS_X]) > 2;
        } else {
            if ((m[Matrix.MTRANS_X] - direction >= 0) ||
                    (m[Matrix.MTRANS_X] - direction - leftBorder) <= 1) {
                canNestedScroll = false;
            }
        }
        return canNestedScroll;
    }

    DraggingListener draggingListener;

    public void setDraggingListener(DraggingListener listener) {
        if (listener != null)
            draggingListener = listener;
    }

    interface DraggingListener {
        void onAlphaChanged(float factor);

        void onResetAlpha();

        boolean needTransition();

        boolean startTransition();
    }
}
