package cn.yudi.imageviewer.viewer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import cn.yudi.imageviewer.model.Message;

class ViewerFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private Message message;

    ViewerFragmentPagerAdapter(FragmentManager fm, Message message) {
        super(fm);
        this.message = message;
    }

    @Override
    public Fragment getItem(int position) {
        return ViewerFragment.newInstance(position, message.getImages().get(position));
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getCount() {
        return message.getImages().size();
    }
}