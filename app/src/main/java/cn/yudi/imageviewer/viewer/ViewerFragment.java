package cn.yudi.imageviewer.viewer;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import cn.yudi.imageviewer.model.Constants;
import cn.yudi.imageviewer.model.Image;
import cn.yudi.imageviewer.widget.YuImageView;
import cn.yudi.imageviewer.widget.YuViewPager;

/**
 * Created by yudi on 2017/2/27.
 */

public class ViewerFragment extends Fragment{
    private Image image;
    private int position;
    private YuImageView imageView;

    static ViewerFragment newInstance(int position, Image image) {
        ViewerFragment fragment = new ViewerFragment();
        fragment.image = image;
        fragment.position = position;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        imageView = new YuImageView(container.getContext());
        imageView.setBackgroundColor(Color.TRANSPARENT);
        imageView.setId(Constants.ids[position]);
        imageView.setImageResource(image.getFakeUrl());
        imageView.setDraggingListener((YuViewPager) container);
        ViewCompat.setTransitionName(imageView, image.getTransitionName());
        return imageView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startPostponedEnterTransition1();
    }


    void startPostponedEnterTransition1() {
        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                ActivityCompat.startPostponedEnterTransition(getActivity());
                return true;
            }
        });
    }
}
