package cn.yudi.imageviewer.viewer;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.util.List;
import java.util.Map;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.Constants;
import cn.yudi.imageviewer.model.Message;
import cn.yudi.imageviewer.model.PositionObservable;
import cn.yudi.imageviewer.widget.YuViewPager;

public class ViewerActivity extends AppCompatActivity implements YuViewPager.OnTransitionListener {

    private YuViewPager viewPager;
    private Message message;
    private int position;
    private int startPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT == 18) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ActivityCompat.postponeEnterTransition(this);

        if (getIntent() != null) {
            message = getIntent().getParcelableExtra(Constants.PROPNAME_DATA);
            position = getIntent().getIntExtra(Constants.PROPNAME_CURRENT_POSITION, 0);
            startPosition = position;
        }
        viewPager = (YuViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new ViewerFragmentPagerAdapter(getSupportFragmentManager(), message));
        viewPager.setCurrentItem(startPosition, false);
        viewPager.setTransitionListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ViewerActivity.this.position = position;
                PositionObservable.getInstance().changePosition(position);
            }
        });
        ActivityCompat.setEnterSharedElementCallback(this, mCallback);
    }

    private final SharedElementCallback mCallback = new SharedElementCallback() {
        @Override
        public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {
            // If mTmpReenterState is null, then the activity is exiting.
            View navigationBar = findViewById(android.R.id.navigationBarBackground);
            View statusBar = findViewById(android.R.id.statusBarBackground);
            if (navigationBar != null) {
                names.add(ViewCompat.getTransitionName(navigationBar));
                sharedElements.put(ViewCompat.getTransitionName(navigationBar), navigationBar);
            }
            if (statusBar != null) {
                names.add(ViewCompat.getTransitionName(statusBar));
                sharedElements.put(ViewCompat.getTransitionName(statusBar), statusBar);
            }
            String startTransitionName = message.getImages().get(startPosition).getTransitionName();
            if (names.contains(startTransitionName)) {
                names.remove(names.indexOf(startTransitionName));
                sharedElements.remove(startTransitionName);
            }
            String transitionName = message.getImages().get(position).getTransitionName();
            names.add(startTransitionName);
            sharedElements.put(transitionName, viewPager.findViewById(Constants.ids[position]));
        }
    };

    @Override
    public void finishAfterTransition() {
        Intent intent = new Intent();
        intent.putExtra(Constants.PROPNAME_CURRENT_POSITION, position);
        setResult(RESULT_OK, intent);
        super.finishAfterTransition();
    }

    @Override
    public void onBackPress() {
        super.onBackPressed();
    }
}
