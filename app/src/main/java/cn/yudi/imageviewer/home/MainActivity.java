package cn.yudi.imageviewer.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.Constants;
import cn.yudi.imageviewer.model.Image;
import cn.yudi.imageviewer.model.Message;
import cn.yudi.imageviewer.model.MessageQueue;

public class MainActivity extends AppCompatActivity implements MainFragmentListener {

    private MainFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT == 18) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_OVERSCAN);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        //getWindow().setStatusBarColor(Color.TRANSPARENT);

        setContentView(R.layout.activity_main);
        // Fake data
        List<Message> messages = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 10 + random.nextInt(5); i++) {
            List<Image> images = new ArrayList<>();
            int count = random.nextInt(4);
            for (int j = 0; j <= count; j++) {
                images.add(new Image(Constants.drawables[random.nextInt(6)], Constants.transitionNames[j]));
            }
            Message message = new Message(images, Constants.texts[new Random().nextInt(Constants.texts.length)]);
            messages.add(message);
        }
        MessageQueue messageQueue = new MessageQueue(messages);
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            fragment = MainFragment.newInstance(messageQueue, this);
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }

    }

    int currentPosition;
    boolean mIsReenter = false;

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
        currentPosition = data.getIntExtra(Constants.PROPNAME_CURRENT_POSITION, 0);
        mIsReenter = true;
        ActivityCompat.postponeEnterTransition(this);
        fragment.scrollToPosition();
        Log.e("reenter", "reenter");
    }

    @Override
    public int getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public boolean isReenter() {
        return mIsReenter;
    }

    @Override
    public void setReenter(boolean reenter) {
        mIsReenter = reenter;
    }
}
