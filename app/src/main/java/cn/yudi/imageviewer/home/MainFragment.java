package cn.yudi.imageviewer.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.Constants;
import cn.yudi.imageviewer.model.Image;
import cn.yudi.imageviewer.model.Message;
import cn.yudi.imageviewer.model.MessageQueue;
import cn.yudi.imageviewer.model.PositionObservable;
import cn.yudi.imageviewer.viewer.ViewerActivity;

public class MainFragment extends Fragment implements HolderListener, Observer {

    private MainFragmentListener listener;
    private MessageQueue messageQueue;
    private RecyclerView recyclerView;

    static MainFragment newInstance(MessageQueue messageQueue, MainFragmentListener listener) {
        MainFragment mainFragment = new MainFragment();
        mainFragment.messageQueue = messageQueue;
        mainFragment.listener = listener;
        PositionObservable.getInstance().addObserver(mainFragment);
        return mainFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new GalleryAdapter(view.getContext(), this, messageQueue));
        ActivityCompat.setExitSharedElementCallback(getActivity(), mCallback);
    }


    private final SharedElementCallback mCallback = new SharedElementCallback() {
        @Override
        public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {
            // for the reason that not transition the navigation bar and status bar
            View navigationBar = getActivity().findViewById(android.R.id.navigationBarBackground);
            View statusBar = getActivity().findViewById(android.R.id.statusBarBackground);
            if (navigationBar != null) {
                names.add(ViewCompat.getTransitionName(navigationBar));
                sharedElements.put(ViewCompat.getTransitionName(navigationBar), navigationBar);
            }
            if (statusBar != null) {
                names.add(ViewCompat.getTransitionName(statusBar));
                sharedElements.put(ViewCompat.getTransitionName(statusBar), statusBar);
            }
            boolean canBeSeen = recyclerView.getLayoutManager().findViewByPosition(viewHolderPosition) != null;
            Log.e("seeeeeen", "canBeSeen : " + canBeSeen);
            if (currentHolder != null && listener.isReenter() && message != null) {
                String transitionName = message.getImages().get(startPosition).getTransitionName();
                if (names.contains(transitionName)) {
                    names.remove(names.indexOf(transitionName));
                    sharedElements.remove(transitionName);
                }
                if (canBeSeen) {
                    RecyclerView.ViewHolder holder = recyclerView.findViewHolderForAdapterPosition(viewHolderPosition);
                    int currentPosition = listener.getCurrentPosition();
                    String newTransitionName = message.getImages().get(currentPosition).getTransitionName();
                    names.add(newTransitionName);
                    currentHolder = (ImagesHolder) holder;
                    sharedElements.put(newTransitionName, currentHolder.itemView.findViewById(Constants.ids[currentPosition]));
                    currentHolder.itemView.findViewById(Constants.ids[currentPosition]).setAlpha(1.F);
                }
            }
        }
    };

    ImagesHolder currentHolder;
    Message message;// We need to save the message because the viewHolder will be recycled
    int startPosition;
    int viewHolderPosition;

    @Override
    public void onClick(ImagesHolder viewHolder, final View clicked, int viewHolderPosition) {
        this.viewHolderPosition = viewHolderPosition;
        listener.setReenter(false);
        message = viewHolder.getMessage();
        // save the data for use in the future
        currentHolder = viewHolder;
        int index = 0;
        for (; index < Constants.ids.length; index++) {
            if (clicked.getId() == Constants.ids[index]) {
                break;
            }
        }
        startPosition = index;
        Image image = message.getImages().get(index);
        Bundle options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), clicked, image.getTransitionName()).toBundle();
        Intent intent = new Intent();
        intent.putExtra(Constants.PROPNAME_DATA, message);
        intent.putExtra(Constants.PROPNAME_CURRENT_POSITION, index);
        intent.setClass(getActivity(), ViewerActivity.class);
        startActivity(intent, options);
    }

    @Override
    public void scrollToPosition() {
        boolean canBeSeen = recyclerView.getLayoutManager().findViewByPosition(viewHolderPosition) != null;
        if (!canBeSeen) {
            recyclerView.scrollToPosition(viewHolderPosition);
            recyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    recyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
                    recyclerView.requestLayout();
                    ActivityCompat.startPostponedEnterTransition(getActivity());
                    return true;
                }
            });
        } else {
            ActivityCompat.startPostponedEnterTransition(getActivity());
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        int currentPosition = (int) arg;
        boolean canBeSeen = recyclerView.getLayoutManager().findViewByPosition(viewHolderPosition) != null;
        if (canBeSeen && startPosition != currentPosition) {
            currentHolder.itemView.findViewById(Constants.ids[startPosition]).setAlpha(1.F);
            currentHolder.itemView.findViewById(Constants.ids[currentPosition]).setAlpha(0F);
            startPosition = currentPosition;
        }
    }
}
