package cn.yudi.imageviewer.home;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.App;
import cn.yudi.imageviewer.model.Constants;
import cn.yudi.imageviewer.model.Image;
import cn.yudi.imageviewer.model.Message;

public class ImagesHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private HolderListener listener;
    private Message message;
    private int viewHolderPosition;


    public ImagesHolder(View itemView, HolderListener listener) {
        super(itemView);
        this.listener = listener;
    }

    Message getMessage() {
        return message;
    }

    void bind(Message message, int position) {
        // We need this parameter to determine whether the holder is still in the main activity
        this.viewHolderPosition = position;
        // We need the message to change from the main fragment to the viewer fragment
        this.message = message;
        // The background
        itemView.setBackgroundColor(position % 2 == 0 ? Color.rgb(230, 230, 230) : Color.rgb(245, 245, 245));
        // The texts
        TextView textView = (TextView) itemView.findViewById(R.id.message);
        textView.setText(message.getTxt());
        // The pictures
        List<Image> images = message.getImages();
        int imageWidth = (Constants.DEVICE_WIDTH - Constants.IMAGE_MARGIN * 4) / 3;
        int imageHeight = imageWidth;
        if (images.size() == 1) {
            // If only one picture, we can just put the
            // picture in a bigger rect to show the picture
            Drawable drawable = App.getApp().getResources().getDrawable(images.get(0).getFakeUrl());
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int maxWidth = Constants.DEVICE_WIDTH - Constants.IMAGE_MARGIN * 2;
            int maxHeight = imageWidth * 3 + Constants.IMAGE_MARGIN * 2;
            float min = Math.min(maxWidth / (0.F + intrinsicWidth), maxHeight / (0.F + intrinsicHeight));
            imageWidth = (int) (min * intrinsicWidth);
            imageHeight = (int) (min * intrinsicHeight);
        }
        for (int i = 0; i < Constants.ids.length; i++) {
            // normalize
            ImageView imageView = (ImageView) itemView.findViewById(Constants.ids[i]);
            imageView.setVisibility(View.GONE);
            imageView.setOnClickListener(null);
            if (i < images.size()) {
                imageView.setVisibility(View.VISIBLE);
                imageView.setOnClickListener(this);
                imageView.setImageResource(images.get(i).getFakeUrl());
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                params.leftMargin = Constants.IMAGE_MARGIN;
                params.width = imageWidth;
                params.height = imageHeight;
                imageView.setLayoutParams(params);
            }
        }
    }

    @Override
    public void onClick(View v) {
        listener.onClick(this, v,viewHolderPosition);
    }
}