package cn.yudi.imageviewer.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.MessageQueue;

class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private HolderListener onClickListener;
    private MessageQueue messageQueue;

    GalleryAdapter(Context context, HolderListener listener, MessageQueue messageQueue) {
        mContext = context;
        onClickListener = listener;
        this.messageQueue = messageQueue;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(mContext).inflate(R.layout.item_gallery, parent, false);
        return new ImagesHolder(root, onClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ImagesHolder) holder).bind(messageQueue.getMessages().get(position), position);

    }

    @Override
    public int getItemCount() {
        return messageQueue.getMessages().size();
    }
}
