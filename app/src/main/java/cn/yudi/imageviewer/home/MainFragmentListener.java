package cn.yudi.imageviewer.home;

/**
 * Created by yudi on 2017/3/1.
 */

interface MainFragmentListener {
    int getCurrentPosition();

    boolean isReenter();

    void setReenter(boolean reenter);
}
