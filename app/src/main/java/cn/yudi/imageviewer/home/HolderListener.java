package cn.yudi.imageviewer.home;

import android.view.View;

/**
 * Created by yudi on 2017/2/28.
 */

interface HolderListener {
    void onClick(ImagesHolder viewHolder, View clicked, int ViewHolderPosition);

    void scrollToPosition();
}
