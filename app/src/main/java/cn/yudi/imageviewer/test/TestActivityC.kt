package cn.yudi.imageviewer.test

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.SharedElementCallback
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.ImageView
import cn.yudi.imageviewer.R
import cn.yudi.imageviewer.R.id.btn_c
import cn.yudi.imageviewer.model.Constants

class TestActivityC: AppCompatActivity() {
    var translationX: Float = 0F
    lateinit var ivImg: ImageView
    var time: Long = System.currentTimeMillis()
    override fun onCreate(savedInstanceState: Bundle?) {
//        window.sharedElementEnterTransition = CustomTranslationXTransition()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b_and_c)
        val btnC = findViewById<Button>(R.id.btn_c)
        btnC.visibility = View.GONE
        translationX = this.intent.getFloatExtra("translationX", 0F)
        val toLong = (Constants.DURATION * (translationX / resources.displayMetrics.widthPixels)).toLong()
//        window.enterTransition = MyChangeBoundsTransition(toLong)
        window.sharedElementEnterTransition = MyChangeBoundsTransition(toLong)

        time = this.intent.getLongExtra("time", System.currentTimeMillis())
        ivImg = findViewById(R.id.iv_img_2)
//        ivImg.translationX = translationX
        ViewCompat.setTransitionName(ivImg, "hehe")
        ActivityCompat.postponeEnterTransition(this)
        ActivityCompat.setEnterSharedElementCallback(this, mCallback)
//        if (translationX != 0F)
        ivImg.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                ivImg.viewTreeObserver.removeOnPreDrawListener(this)
                ActivityCompat.startPostponedEnterTransition(this@TestActivityC)
                return true
            }
        })
    }

    private val mCallback = object : SharedElementCallback() {
        override fun onSharedElementStart(sharedElementNames: MutableList<String>?, sharedElements: MutableList<View>?, sharedElementSnapshots: MutableList<View>?) {
            super.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots)
            sharedElements!!.forEach {
                if (it == ivImg) {
                    ivImg.tag = System.currentTimeMillis() - time
                }
            }

        }

        override fun onSharedElementEnd(sharedElementNames: MutableList<String>?, sharedElements: MutableList<View>?, sharedElementSnapshots: MutableList<View>?) {
            super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots)
            sharedElements!!.forEach {
                if (it == ivImg) {
                    ivImg.tag = null
                }
            }
        }

        override fun onMapSharedElements(names: MutableList<String?>, sharedElements: MutableMap<String, View>?) {
            // If mTmpReenterState is null, then the activity is exiting.
            val navigationBar = findViewById<View>(android.R.id.navigationBarBackground)
            val statusBar = findViewById<View>(android.R.id.statusBarBackground)
            if (navigationBar != null) {
                names.add(ViewCompat.getTransitionName(navigationBar))
                sharedElements!![ViewCompat.getTransitionName(navigationBar)!!] = navigationBar
            }
            if (statusBar != null) {
                names.add(ViewCompat.getTransitionName(statusBar))
                sharedElements!![ViewCompat.getTransitionName(statusBar)!!] = statusBar
            }
            val startTransitionName = "hehe"
            if (names.contains(startTransitionName)) {
                names.removeAt(names.indexOf(startTransitionName))
                sharedElements!!.remove(startTransitionName)
            }
//            val transitionName = message.getImages().get(position).getTransitionName()
            names.add(startTransitionName)
            sharedElements!![startTransitionName] = ivImg
//            sharedElements!![transitionName] = viewPager.findViewById(Constants.ids[position])
        }
    }
}