package cn.yudi.imageviewer.test;

import android.animation.Animator;
import android.animation.FloatEvaluator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import cn.yudi.imageviewer.R;
import cn.yudi.imageviewer.model.Constants;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class CustomTranslationXTransition extends Transition {
    private static final String PROPNAME_TRANSLATIONX = "yudixiaomie:TranslationX";
    private static final String[] TRANSITION_PROPERTIES = {PROPNAME_TRANSLATIONX};

    public CustomTranslationXTransition() {
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomTranslationXTransition(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Nullable
    @Override
    public String[] getTransitionProperties() {
        return TRANSITION_PROPERTIES;
    }

    @Override
    public void captureEndValues(@NonNull TransitionValues transitionValues) {
        Log.e("captureEndValues", "translationX: " + transitionValues.view.getTranslationX() +
                ",left:  " + transitionValues.view.getLeft() + ", id: " + transitionValues.view.getId());
        captureValues(transitionValues);
    }

    @Override
    public void captureStartValues(@NonNull TransitionValues transitionValues) {
        Log.e("captureStartValues", "translationX: " + transitionValues.view.getTranslationX() +
                ",left:  " + transitionValues.view.getLeft() + ", id: " + transitionValues.view.getId());
        captureValues(transitionValues);
    }

    private void captureValues(@NonNull TransitionValues transitionValues) {
//        if (transitionValues.view.getTag() != null) {
//            Log.e("captureValues", "translationX: " + transitionValues.view.getTag());
//            transitionValues.values.put(PROPNAME_TRANSLATIONX, ((Float) transitionValues.view.getTag()));
//        } else
        float comsumed = 0F;
        if (transitionValues.view.getTag() instanceof Long) {
            comsumed = ((long) transitionValues.view.getTag() / Constants.DURATION) * transitionValues.view.getContext().getResources()
                    .getDisplayMetrics().widthPixels;
        }
        Log.e("captureValues", "comsumed: " + comsumed);
        if (transitionValues.view.getId() == R.id.iv_img_1
                || transitionValues.view.getId() == R.id.iv_img_2)
            transitionValues.values.put(PROPNAME_TRANSLATIONX, transitionValues.view.getTranslationX() + transitionValues.view.getLeft() - comsumed);
    }

    @Nullable
    @Override
    public Animator createAnimator(@NonNull ViewGroup sceneRoot, @Nullable TransitionValues startValues,
                                   @Nullable TransitionValues endValues) {
        if (startValues == null || endValues == null) {
            return null;
        }

        final Float startTranslationX = (Float) startValues.values.get(PROPNAME_TRANSLATIONX);
        final Float endTranslationX = (Float) endValues.values.get(PROPNAME_TRANSLATIONX);
        Log.e("createAnimator", "start translationX: " + startTranslationX +
                ",end translationX:  " + endTranslationX);
        final View view = endValues.view;
        final FloatEvaluator argbEvaluator = new FloatEvaluator();
        view.setTranslationX(startTranslationX);

        ValueAnimator animator = ValueAnimator.ofFloat(0, 1.0f);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration((long) (Constants.DURATION * (startTranslationX / view.getContext().getResources().getDisplayMetrics().widthPixels)));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float color = argbEvaluator.evaluate(animation.getAnimatedFraction(), startTranslationX, endTranslationX);
                Log.e("onAnimationUpdate", "translationX: " + color +
                        " view's left: " + view.getLeft() + ",view's translationX: " + view.getTranslationX() + ", id: " + view.getId());
                view.setTranslationX(color);
            }
        });

        return animator;
    }
}