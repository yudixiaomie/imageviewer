package cn.yudi.imageviewer.test

import android.animation.Animator
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.transition.ChangeBounds
import android.transition.TransitionValues
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator

class MyChangeBoundsTransition(val newduration: Long) : ChangeBounds() {

    override fun createAnimator(sceneRoot: ViewGroup?, startValues: TransitionValues?, endValues: TransitionValues?): Animator {
        val animator = super.createAnimator(sceneRoot, startValues, endValues)
        animator.duration = newduration
        animator.interpolator = LinearInterpolator()
        return animator
    }
}