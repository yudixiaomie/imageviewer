package cn.yudi.imageviewer.test

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.widget.ImageView

class LogImageView : ImageView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun setTranslationX(translationX: Float) {
        super.setTranslationX(translationX)
        Log.e("LogImageView : ", "  translationX$translationX left$left ${this.id}")
    }
}