package cn.yudi.imageviewer.test

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.SharedElementCallback
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import cn.yudi.imageviewer.R
import cn.yudi.imageviewer.model.Constants

class TestActivityA : AppCompatActivity() {
    private lateinit var ivImg: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a)
        val btnB = findViewById<Button>(R.id.btn_b)
        val start = findViewById<Button>(R.id.btn_start_animation)
        ivImg = findViewById<ImageView>(R.id.iv_img_1)
        ivImg.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                ivImg.translationX = ivImg.width.toFloat()
                ivImg.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
        start.setOnClickListener {
            ivImg.translationX = ivImg.width.toFloat()
            ivImg.animate().translationX(0F).setDuration(Constants.DURATION).setUpdateListener {
                Log.e("ivImg", "animate().translationX translationX: ${ivImg.translationX}")
            }.start()
        }
//        ivImg.translationX = ivImg.width.toFloat()/2
        btnB.setOnClickListener {
            ivImg.clearAnimation()
            val consumed = 30 / Constants.DURATION * ivImg.context.resources
                    .displayMetrics.widthPixels
            ivImg.animate().translationX(ivImg.translationX - (consumed)).setDuration(40).start()

            val intent = Intent()
            Toast.makeText(this, ivImg.translationX.toString(), Toast.LENGTH_SHORT).show()
            intent.putExtra("translationX", ivImg.translationX)
            intent.putExtra("time", System.currentTimeMillis())
            intent.setClass(this, TestActivityB::class.java)
//            startActivity(intent,
//                    ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle())
            startActivity(intent)//, options)
        }
        ActivityCompat.setExitSharedElementCallback(this, mCallback)

    }

    override fun startActivity(intent: Intent?) {
        this.startActivity(intent, Bundle())
    }

    override fun startActivity(intent: Intent?, options: Bundle?) {
        options?.putAll(ActivityOptionsCompat.makeSceneTransitionAnimation(this, ivImg, "hehe").toBundle())
        super.startActivity(intent, options)
    }


    private val mCallback = object : SharedElementCallback() {
        override fun onMapSharedElements(names: MutableList<String?>, sharedElements: MutableMap<String, View>?) {
            // for the reason that not transition the navigation bar and status bar
            val navigationBar = this@TestActivityA.findViewById<View>(android.R.id.navigationBarBackground)
            val statusBar = this@TestActivityA.findViewById<View>(android.R.id.statusBarBackground)
            if (navigationBar != null) {
                names!!.add(ViewCompat.getTransitionName(navigationBar!!))
                sharedElements!![ViewCompat.getTransitionName(navigationBar!!)!!] = navigationBar!!
            }
            if (statusBar != null) {
                names!!.add(ViewCompat.getTransitionName(statusBar!!))
                sharedElements!![ViewCompat.getTransitionName(statusBar!!)!!] = statusBar!!
            }
            names.add("hehe")
            sharedElements!!["hehe"] = ivImg
        }
    }
}