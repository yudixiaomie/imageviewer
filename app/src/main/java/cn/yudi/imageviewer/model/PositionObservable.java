package cn.yudi.imageviewer.model;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by yudi on 2017/3/3.
 */

public class PositionObservable extends Observable {
    private static PositionObservable instance = new PositionObservable();

    private PositionObservable() {
    }

    public   void changePosition(int position) {
        setChanged();
        notifyObservers(position);
    }

    public static PositionObservable getInstance() {
        return instance;
    }
}
