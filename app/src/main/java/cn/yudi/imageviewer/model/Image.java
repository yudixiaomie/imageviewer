package cn.yudi.imageviewer.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {
    private String stringUrl;
    private int fakeUrl;
    private String transitionName;

    public Image(String url, String transitionName) {
        this.stringUrl = url;
        this.transitionName = transitionName;
    }

    public Image(int fakeUrl, String transitionName) {
        this.fakeUrl = fakeUrl;
        this.transitionName = transitionName;
    }

    public int getFakeUrl() {
        return fakeUrl;
    }

    public void setFakeUrl(int fakeUrl) {
        this.fakeUrl = fakeUrl;
    }

    public String getStringUrl() {
        return stringUrl;
    }

    public void setStringUrl(String stringUrl) {
        this.stringUrl = stringUrl;
    }

    public String getTransitionName() {
        return transitionName;
    }

    public void setTransitionName(String transitionName) {
        this.transitionName = transitionName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.stringUrl);
        dest.writeInt(this.fakeUrl);
        dest.writeString(this.transitionName);
    }

    protected Image(Parcel in) {
        this.stringUrl = in.readString();
        this.fakeUrl = in.readInt();
        this.transitionName = in.readString();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
