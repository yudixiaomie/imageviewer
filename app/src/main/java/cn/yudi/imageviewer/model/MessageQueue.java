package cn.yudi.imageviewer.model;

import java.util.List;

public class MessageQueue {
    private List<Message> messages;

    public MessageQueue(List<Message> messages) {
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
