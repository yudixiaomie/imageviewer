package cn.yudi.imageviewer.model;

import cn.yudi.imageviewer.R;

/**
 * Created by yudi on 2017/3/1.
 */

public class Constants {

    public static final String PROPNAME_CURRENT_POSITION = "cn.yudi.imageviewer:maimfragment:propname_current_position";
    public static final String PROPNAME_DATA = "cn.yudi.imageviewer:maimfragment:propname_data";

    public static final int[] ids = {
            R.id.image1, R.id.image2, R.id.image3,
            R.id.image4, R.id.image5, R.id.image6,
            R.id.image7, R.id.image8, R.id.image9
    };

    public static final int[] drawables = {
            R.drawable.banner, R.drawable.comic, R.drawable.fox,
            R.drawable.pikachu, R.drawable.ss, R.drawable.xx
    };

    public static final String[] texts = {
            "我独自在横跨过田地的路上走着，\n" +
                    "　　夕阳像一个守财奴似的，\n" +
                    "　　正藏起它的最后的金子。", "Eyes are raining for her,\nheart is holding umbrella for her,\nthis is love", "我一定要知道。\n" +
            "　　我一定要找到她，\n把她锁起来。",
            "Let life be beautiful like summer flowers and deathlike autumn leaves", "如果你因失去了太阳而流泪，\n那么你也将失去群星了。", "你微微地笑着，\n不同我说什么话。\n而我觉得，为了这个，\n我已等待得久了。\n    You smiled and talked to me of nothing and I feltthat for this I had been waiting long",
            "6、《水手》\n" +
                    "\n" +
                    "　　船夫曼特胡的船只停泊在拉琪根琪码头。\n" +
                    "　　这只船无用地装载着黄麻，\n" +
                    "　　无所事事地停泊在那里已经好久了。", "We come nearest to the great when we are great inhumility。 ", "轻轻的我走了, \n正如我轻轻的来; \n我轻轻的招手, \n作别西天的云彩。"
    };

    public static final String[] transitionNames = {
            "image1", "image2", "image3",
            "image4", "image5", "image6",
            "image7", "image8", "image9"
    };

    public static int IMAGE_MARGIN = (int) (App.getApp().getResources().getDisplayMetrics().density * 20);
    public static int DEVICE_WIDTH = App.getApp().getResources().getDisplayMetrics().widthPixels;
    public static int DEVICE_HEIGHT = App.getApp().getResources().getDisplayMetrics().heightPixels;


    public static long DURATION = 6000;

}
