package cn.yudi.imageviewer.model;

import android.app.Application;
import android.content.Context;

/**
 * Created by yudi on 2017/3/2.
 */

public class App extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getApp() {
        return mContext;
    }
}
